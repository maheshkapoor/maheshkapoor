
module.exports = function(config) {
    
  config.set({

    basePath: '',
    frameworks: ['jasmine'],

    files: [
      'bower_components/jquery/dist/jquery.js',
      'build/lib/angular.min.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'build/*.js',
      'test/*.test.js'
    ],


    reporters: ['progress'],


    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false
  })
}