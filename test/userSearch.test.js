describe('userSearch', function () {

    beforeEach(module('userSearch'));
    var $controller, $filter;
    beforeEach(
        inject(function(_$controller_, _$filter_){
            $controller = _$controller_;
            $filter = _$filter_;
        }));
    
    describe('searchByInput', function () {
        var $scope, controller;

        beforeEach(function() {
        $scope = {};
        controller = $controller('mainController', { $scope: $scope });

        });


      it('should display first names with an "a"', function () {
            var Result = [{
                              'id': '1',
                              'firstName': 'Sean',
                              'lastName': 'Kerr',
                              'picture': 'img/sean.jpg',
                              'Title': 'Senior Developer'
                          },
                          {
                              'id': '2',
                              'firstName': 'Yaw',
                              'lastName': 'Ly',
                              'picture': 'img/yaw.jpg',
                              'Title': 'AEM Magician'
                          },    
                          {
                              'id': '6',
                              'firstName': 'Hayley',
                              'lastName': 'Crimmins',
                              'picture': 'img/hayley.jpg',
                              'Title': 'Dev Manager'
                          }];

            var ActualResult = $filter('filter')($scope.persons, {firstName:"a"});
            expect(ActualResult).toEqual(Result);
            
        });

      it('Should search two letters not matching case', function () {
            var Result = [{
                              'id': '3',
                              'firstName': 'Lucy',
                              'lastName': 'Hehir',
                              'picture': 'img/lucy.jpg',
                              'Title': 'Scrum master'
                          }];
             
            var ActualResult = $filter('filter')($scope.persons, {firstName:"lu"});
            expect(ActualResult).toEqual(Result);
          });


      it('Should search first name not matching case', function () {
            var Result = [{               
                        'id': '5',
                        'firstName': 'Eric',
                        'lastName': 'Kwok',
                        'picture': 'img/eric.jpg',
                        'Title': 'Technical Lead'
                    }];
                    
          var ActualResult = $filter('filter')($scope.persons, {firstName:"eric"});
          expect(ActualResult).toEqual(Result);
          });

    <!--negative test case -->
      it('Should not display anybody', function () {
            var Result = [];
            var ActualResult = $filter('filter')($scope.persons, {firstName:"mm"});
            expect(ActualResult).toEqual(Result);
            
        });

    });

});